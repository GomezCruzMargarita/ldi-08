/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package Programas;

import java.util.Scanner;

/**
 *
 * @author Margarita
 */
public class ConvertirCadenaMenu {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
       int op;
       
       Scanner entrada = new Scanner(System.in);
      do{
       System.out.println("1.- Convertir cadena de texto a Decimal");
       System.out.println("2.- Convertir cadena de texto a Binario");
       System.out.println("3.- Salir");
       op=entrada.nextInt();
       
       switch(op) 
       {
           case 1:
               String cadena;
               System.out.println("Ingresa la cadena de texto: ");
               cadena = entrada.next();
               
                byte b;
                System.out.println("Cadena de texto "+ cadena + " convertida a decimal es: ");
                for(int i=0; i<cadena.length();i++){
                  b = (byte)cadena.charAt(i);
                 System.out.println(b);
                }
               break;
           case 2:
               String cadena1;
                       
               System.out.println("Ingresa la cadena de texto: ");
               cadena1 = entrada.next();
                 String textoBinario = "";
                 System.out.println("Cadena de texto "+ cadena1 + " convertida a binario es:");
                for(char letra : cadena1.toCharArray())
                {
                     textoBinario += String.format("%8s", Integer.toBinaryString(letra));
            
                 }
                    System.out.println(textoBinario);
             
       }
      }while(op != 3);
    }
    
}
